export interface Message {
  body: string;
  title: string;
}

export interface DashboardProps {
  message: Message;
}
