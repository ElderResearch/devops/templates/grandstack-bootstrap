import { useState, useEffect } from 'react';
import { Message } from '../types';

export const useDashboard = () => {
  const [message, setMessage] = useState<Message | null>(null);

  const fetchMessage = async () => {
    const domain = window.location.origin || 'localhost';

    setMessage({
      title: 'Welcome to dashboard!',
      body: `This message is coming from ${domain}`,
    });
  };

  useEffect(() => {
    fetchMessage();
  }, []);

  return {
    message,
  };
};
