import { Dashboard } from '../components';
import { useDashboard } from '../hooks';

export const DashboardPage = () => {
  const { message } = useDashboard();

  if (!message) return <div>Loading...</div>; // TODO: Replace with loader from design-system

  return <Dashboard message={message} />;
};
