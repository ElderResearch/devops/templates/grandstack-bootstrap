import { Box, Typography } from '@design-system';
import { DashboardProps } from '../types';

export const Dashboard = ({ message }: DashboardProps) => (
  <Box
    sx={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      height: '100%',
    }}
  >
    <Typography variant="h1">{message.title}</Typography>
    <Typography>{message.body}</Typography>
  </Box>
);
