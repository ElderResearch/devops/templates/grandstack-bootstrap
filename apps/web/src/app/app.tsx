// eslint-disable-next-line @typescript-eslint/no-unused-vars
import styles from './app.module.css';

import { AppRouter } from './AppRouter';

export function App() {
  return <AppRouter />;
}

export default App;
