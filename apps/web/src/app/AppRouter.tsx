import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { DashboardRoutes } from '@grandstack-bootstrap/feat-dashboard';

export const AppRouter = () => {
  return (
    <Router>
      <Routes>
        <Route path="/*" element={<DashboardRoutes />} />
      </Routes>
    </Router>
  );
};
